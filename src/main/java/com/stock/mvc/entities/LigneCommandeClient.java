package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class LigneCommandeClient implements Serializable{
	@Id
	@GeneratedValue
	private Long IdLCC;

	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="idCommandeClient")
	private CommandeClient commandeClient;
	
	public LigneCommandeClient() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public Article getArticle() {
		return article;
	}


	public void setArticle(Article article) {
		this.article = article;
	}


	public CommandeClient getCommandeClient() {
		return commandeClient;
	}


	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}


	public Long getIdLCC() {
		return IdLCC;
	}

	public void setIdLCC(Long idLCC) {
		IdLCC = idLCC;
	}
	
	
}
