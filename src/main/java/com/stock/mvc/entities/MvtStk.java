package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

@Entity
public class MvtStk implements Serializable{
	
	public static final int ENTREE = 1;
	public static final int SORTIE = 2;
	
	@Id
	@GeneratedValue
	private Long idMvtStk;

	@Temporal(TemporalType.TIMESTAMP)
	private Date mvmStk;
	
	private BigDecimal quantite;
	
	private int typeMvt;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	
	public MvtStk() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Long getIdMvtStk() {
		return idMvtStk;
	}


	public void setIdMvtStk(Long idMvtStk) {
		this.idMvtStk = idMvtStk;
	}


	public Date getMvmStk() {
		return mvmStk;
	}


	public void setMvmStk(Date mvmStk) {
		this.mvmStk = mvmStk;
	}


	public BigDecimal getQuantite() {
		return quantite;
	}


	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}


	public int getTypeMvt() {
		return typeMvt;
	}


	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}


	public Article getArticle() {
		return article;
	}


	public void setArticle(Article article) {
		this.article = article;
	}


	public static int getEntree() {
		return ENTREE;
	}


	public static int getSortie() {
		return SORTIE;
	}

	
}

