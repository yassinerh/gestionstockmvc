package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class LigneCommandeFournisseur implements Serializable{
	@Id
	@GeneratedValue
	private Long idLCF;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="idCommandeFournisseur")
	private CommandeFournisseur commandeFournisseur;
	
	public LigneCommandeFournisseur() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}

	public Long getIdLCF() {
		return idLCF;
	}

	public void setIdLCF(Long idLCF) {
		this.idLCF = idLCF;
	}
		
}
