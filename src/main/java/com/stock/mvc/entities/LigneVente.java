package com.stock.mvc.entities;

import java.io.Serializable;
import javax.persistence.*;

@Entity
public class LigneVente implements Serializable{
	@Id
	@GeneratedValue
	private Long idLVente;

	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="idVente")
	private Vente vente;
	
	public LigneVente() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Article getArticle() {
		return article;
	}



	public void setArticle(Article article) {
		this.article = article;
	}



	public Vente getVente() {
		return vente;
	}



	public void setVente(Vente vente) {
		this.vente = vente;
	}



	public Long getIdLVente() {
		return idLVente;
	}

	public void setIdLVente(Long idLVente) {
		this.idLVente = idLVente;
	}
	
	
}
