package com.stock.mvc.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.stock.mvc.dao.IDAOGenerique;

@SuppressWarnings("unchecked")
public class DAOGeneriqueImp<E> implements IDAOGenerique<E> {
	@PersistenceContext
	EntityManager em;
	
	private Class<E> type;
	
	public Class<E> getType(){
		return type;
	}
	public DAOGeneriqueImp() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class<E>) pt.getActualTypeArguments()[0];
	}

	@Override
	public E save(E entity) {
		// TODO Auto-generated method stub
		em.persist(entity);
		return entity;
	}

	@Override
	public E update(E entity) {
		// TODO Auto-generated method stub
		return em.merge(entity);
	}

	@Override
	public List<E> selectAll() {
		Query query = em.createQuery("select t from "+type.getSimpleName()+" t");
		return query.getResultList();
	}

	@Override
	public List<E> selectAll(String sortField, String sort) {
		Query query = em.createQuery("select t from "+type.getSimpleName()+" t order by "+ sortField + " " + sort );
		return query.getResultList();
	}

	@Override
	public E getById(Long id) {
		// TODO Auto-generated method stub
		return em.find(type, id);
	}

	@Override
	public void removeById(Long id) {
		// TODO Auto-generated method stub
		E tab = em.getReference(type, id);
		em.remove(tab);
	}

	@Override
	public E findOne(String paramName, Object paramValue) {
		// c'est du JPQL
		Query query = em.createQuery("select t from "+type.getSimpleName()+" t where "+ paramName + " = :x");
		query.setParameter(paramName, paramValue);
		return query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null;
	}

	@Override
	public E findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		String queryString = "select e from "+type.getSimpleName()+" e where ";
		int len = paramNames.length;
		for(int i=0;i<len;i++) {
			queryString += " e." + paramNames[i] + " = :x"+ i ;
			if((i+1) < len)
				queryString += " and ";
		}
		Query query = em.createQuery(queryString);
		for(int i=0;i<len;i++) {
			query.setParameter("x"+i, paramValues[i]);	
		}

		return query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null;
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		Query query = em.createQuery("select t from "+type.getSimpleName()+" t where "+ paramName + " = :x");
		query.setParameter(paramName, paramValue);
		return query.getResultList().size() > 0 ? ( (Long)query.getSingleResult() ).intValue() : 0;
	}

}
